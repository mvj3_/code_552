建议使用该方法获取图片
NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"sun"  ofType:@"png"];
UIImage *image = [[UIImage alloc] initWithContentsOfFile:imagePath];
  
不要使用下面的方法，图片过大容易造成内存溢出
UIImage *image = [UIImage imageNamed:@"sun.png"];